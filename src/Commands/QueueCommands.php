<?php

namespace Drupal\parallel_queue\Commands;

use Drupal;
use Drupal\Core\Database\Database;
use Drush\Drupal\Commands\core\QueueCommands as DrushQueueCommands;

/**
 * A parallel process replacement for the core Drush queue processor.
 *
 * This uses pcntl to create sub-process forks, then delegates the processing
 * to each sub-process. Items are evenly distributed between processes.
 */
class QueueCommands extends DrushQueueCommands {

  /**
   * Replace the queue:run command provided by Drush core.
   *
   * @hook replace-command queue:run
   *
   * @param string $name The name of the queue to run, as defined in either hook_queue_info or hook_cron_queue_info.
   * @validate-queue name
   * @option time-limit The maximum number of seconds allowed to run the queue
   * @option items-limit The maximum number of items allowed to run the queue
   * @option process-limit The maximum number of sub-processes allowed to run the queue
   */
  public function run($name, $options = [
    'time-limit' => self::REQ,
    'items-limit' => self::OPT,
    'process-limit' => self::OPT,
  ]) {
    // Default to no parallel processing.
    // @TODO: Allow a configurable default?
    if (empty($options['process-limit'])) {
      $options['process-limit'] = 1;
    }

    // Don't allow the number of sub-processes to exceed the number of items.
    if (!empty($options['items-limit']) && ($options['items-limit'] < $options['process-limit'])) {
      $options['process-limit'] = $options['items-limit'];
    }

    // @see self::forkDatabase().
    Database::closeConnection();

    $pids = [];
    for ($i = 1; $i <= $options['process-limit']; $i++) {
      $pid = pcntl_fork();
      switch ($pid) {
        // This is a failure condition: the child process could not be created.
        case -1:
          throw new \Exception('Process forking is not available.');

        // This is a Child process – call parent::run to evaluate the queue.
        case 0:
          $this->forkDatabase();
          if (!empty($options['items-limit'])) {
            $options['items-limit'] = $this->getItemLimit($options['items-limit'], $options['process-limit'], $i);
          }
          return parent::run($name, $options);

        // This is the Parent process – track the process IDs of each child.
        default:
          $pids[$i] = $pid;
          break;
      }
    }

    // Wait for each child process to complete.
    foreach ($pids as $pid) {
      pcntl_waitpid($pid, $status);
    }
  }

  /**
   * Create a new database connection.
   *
   * If the PDO connections are shared amongst sub-processes, when the first
   * sub-process exits, the PDO connection will be closed for all the
   * sub-processes.
   *
   * This ensures each sub-process maintains a separate database connection.
   */
  public function forkDatabase() {
    $connection = Database::getConnection();
    // Update the DI container.
    Drupal::getContainer()->set('database', $connection);
  }

  /**
   * Get the item-limit for a particular run.
   *
   * @param int $items_limit
   *   The total number of items to process.
   * @param int $process_limit
   *   The number of sub-processes allowed.
   * @param int $i
   *   The sequence in the sub-process forks.
   *
   * @return int
   *   The number of items permitted in a particular sub-process.
   */
  protected function getItemLimit($items_limit, $process_limit, $i) {
    // Optimize for filling each process – e.g. for 100 items and 9 processes:
    // - 8 processes with 11 items, and 1 process with 12 items,
    //   rather than:
    // - 8 processes with 12 items, and 1 process with 4 items.
    $items_per_process = floor($items_limit / $process_limit);
    $remainder = $items_limit % $items_per_process;
    return ($i == $process_limit)
      ? ($items_per_process + $remainder)
      : $items_per_process;
  }

}
